const app = require('../../src/app');
//const assert = require('assert');

describe('\'books\' service', () => {
  it('User cannot delete book of other user', async () => {
    const user1 = await app.service('users').create({
      name: 'Test',
      email: 'messagetest@example.com',
      password: '123456'
    });
    const user2 = await app.service('users').create({
      name: 'Test2',
      email: 'messagetest2@example.com',
      password: '12345678'
    });
    const book1 = await app.service('books').create({
      author: 'Test',
      title: 'Test',
      u_id: user2.id
    });

    try {
      await app.service('books').remove(book1.id, { user: user1 });
    } catch (e) {
      expect(e.name).toBe('NotFound');
    }
  });

  it('User can delete his book only', async () => {
    const user2 = await app.service('users').create({
      name: 'Test2',
      email: 'messagetest2@example.com',
      password: '12345678'
    });
    const book1 = await app.service('books').create({
      author: 'Test',
      title: 'Test',
      u_id: user2.id
    });

    try {
      const book = await app.service('books').remove(book1.id, { user: user2 });
      expect(book.id).toBe(book1.id);
    } catch (e) {
      expect(e.name).toBe('NotFound');
    }
  });

  it('User cannot update(PUT) others book', async () => {
    //4
    const user1 = await app.service('users').create({
      name: 'Test',
      email: 'messagetest@example.com',
      password: '123456'
    });
    const user2 = await app.service('users').create({
      name: 'Test2',
      email: 'messagetest2@example.com',
      password: '12345678'
    });
    const book1 = await app.service('books').create({
      author: 'Test',
      title: 'Test',
      u_id: user2.id
    });
    try {
      // const params = { user1 };
      await app.service('books').update(
        book1.id,
        {
          author: 'Test',
          title: 'Test2'
        },
        { user: user1 }
      );
    } catch (e) {
      expect(e.name).toBe('NotFound');
    }
  });

  it('User can update(PUT) his book only', async () => {
    const user2 = await app.service('users').create({
      name: 'Test2',
      email: 'messagetest2@example.com',
      password: '12345678'
    });
    const book1 = await app.service('books').create({
      author: 'Test',
      title: 'Test',
      u_id: user2.id
    });
    try {
      // const params = { user1 };
      const book = await app.service('books').update(
        book1.id,
        {
          author: 'Test',
          title: 'Test5',
          u_id: user2.id
        },
        { user: user2 }
      );
      expect(book.id).toBe(book1.id);
    } catch (e) {
      expect(e.name).toBe('NotFound');
    }
  });

  it('User cannot update multiple data entries(PATCH) of other users book', async () => {
    //4
    const user1 = await app.service('users').create({
      name: 'Test',
      email: 'messagetest@example.com',
      password: '123456'
    });
    const user2 = await app.service('users').create({
      name: 'Test2',
      email: 'messagetest2@example.com',
      password: '12345678'
    });
    const book1 = await app.service('books').create({
      author: 'Test',
      title: 'Test',
      u_id: user2.id
    });
    try {
      // const params = { user1 };
      await app.service('books').patch(
        book1.id,
        {
          author: 'Test2',
          title: 'Test2',
          u_id: 3
        },
        { user: user1 }
      );
    } catch (e) {
      expect(e.name).toBe('NotFound');
    }
  });

  it('User can update multiple data entries(PATCH) of his books only', async () => {
    //4
    const user1 = await app.service('users').create({
      name: 'Test',
      email: 'messagetest@example.com',
      password: '123456'
    });

    const book1 = await app.service('books').create({
      author: 'Test',
      title: 'Test',
      u_id: user1.id
    });

    try {
      // const params = { user1 };
      const book = await app.service('books').patch(
        book1.id,
        {
          author: 'Test3',
          title: 'Test3',
          u_id: 3
        },
        { user: user1 }
      );
      expect(book.id).toBe(book1.id);
    } catch (e) {
      expect(e.name).toBe('NotFound');
    }
  });
});
