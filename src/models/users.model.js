// See https://vincit.github.io/objection.js/#models
// for more of what you can do here.
const { Model } = require('objection');

class Users extends Model {
  static get tableName() {
    return 'users';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name', 'email', 'password'],

      properties: {
        name: { type: ['string'] },
        email: { type: ['string'] },
        password: 'string'
      }
    };
  }
}

module.exports = function(app) {
  const db = app.get('knex');

  db.schema
    .hasTable('users')
    .then(exists => {
      if (!exists) {
        db.schema
          .createTable('users', table => {
            table.increments('id');

            table.string('email').unique();
          })
          .then(() => console.log('Created users table')) // eslint-disable-line no-console
          .catch(e => console.error('Error creating users table', e)); // eslint-disable-line no-console
      }
    })
    .catch(e => console.error('Error creating users table', e)); // eslint-disable-line no-console

  return Users;
};
