const { authenticate } = require('@feathersjs/authentication').hooks;

const { setField } = require('feathers-authentication-hooks');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [
      authenticate('jwt'),
      setField({
        from: 'params.user.id',
        as: 'params.query.u_id'
      })
    ],
    patch: [
      authenticate('jwt'),
      setField({
        from: 'params.user.id',
        as: 'params.query.u_id'
      })
    ],
    remove: [
      authenticate('jwt'),
      setField({
        from: 'params.user.id',
        as: 'params.query.u_id'
      })
    ]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
