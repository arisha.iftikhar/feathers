// Update with your config settings.
var options = {
  development: {
    client: "pg",
    connection: "postgres://postgres:areesha@localhost:5432/postgres",
    migrations: {
      directory: __dirname + "/db/migrations"
    },
    seeds: {
      directory: __dirname + "/db/seeds"
    }
  },
  production: {
    client: "pg",
    connection: process.env.DATABASE_URL,
    migrations: {
      directory: __dirname + "/db/migrations"
    },
    seeds: {
      directory: __dirname + "/db/seeds/production"
    }
  }
};

var environment = process.env.NODE_ENV || "development";
var config = options[environment];
module.exports = config;
