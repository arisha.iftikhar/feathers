// Establish a Socket.io connection
const socket = io();
// Initialize our Feathers client application through Socket.io
// with hooks and authentication.
const client = feathers();

client.configure(feathers.socketio(socket));

// Use localStorage to store our login token
client.configure(feathers.authentication());

// Login screen
const loginHTML = `<main class="login container">
  <div class="row">
    <div class="col-12 col-6-tablet push-3-tablet text-center heading">
      <h1 class="font-100">Login</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-6-tablet push-3-tablet col-4-desktop push-4-desktop">
      <form class="form" >
        <fieldset>
          Email:- <input class="block" type="email" name="email" placeholder="email">
        </fieldset>
        <fieldset>
          Password:- <input class="block" type="password" name="password" placeholder="password">
        </fieldset>
        <button type="button" id="login" class="button button-primary block signup">
          Login
        </button>
        <a href="#" id="signupPage">SignUp</a>
      </form>
    </div>
  </div>
</main>`;

// Chat base HTML (without user list and messages)
const signupHTML = `<main class="login container">
<div class="row">
  <div class="col-12 col-6-tablet push-3-tablet text-center heading">
    <h1 class="font-100">Signup</h1>
  </div>
</div>
<div class="row">
  <div class="col-12 col-6-tablet push-3-tablet col-4-desktop push-4-desktop">
    <form class="form">
        Name:- <input class="block" type="text" name="name" placeholder="name" required><br />
        Email:- <input class="block" type="email" name="email" placeholder="email" required><br />
        Password:- <input class="block" type="password" name="password" placeholder="password" required><br />
      <button type="button" id="signup2" class="button button-primary block signup">
        Sign up 
      </button>
      <a href="#" id="login2">Login</a>
    </form>
  </div>
</div>
</main>`;

// User HTML
const userHTML = `<main class="flex flex-column">
  <header class="title-bar flex flex-row flex-center">
    <div class="title-wrapper block center-element">
      <span class="title">Users</span>
    </div>
  </header>
  <div class="flex flex-row flex-1 clear">
    <aside class="sidebar col col-3 flex flex-column flex-space-between">
      <header class="flex flex-row flex-center">
        <h4 class="font-300 text-center">
          <span class="font-600 onlineCount">0</span> Users
        </h4>
      </header>
      <ul class="flex flex-column flex-1 list-unstyled userList"></ul>
      <br />
      <span class="title">Books</span>
      <h4 class="font-300 text-center">
          <span class="font-600 onlineCount2">0</span> Books
        </h4>
      <ul class="flex flex-column flex-1 list-unstyled bookList"></ul>
      <form class="form">
      Title:- <input class="block" type="text" name="title" placeholder="title"><br />
      Author:- <input class="block" type="text" name="author" placeholder="author"><br />
      User Id:- <input class="block" type="number" name="userId" placeholder="User Id"><br />
    <button type="button" id="addBook" class="button button-primary block signup">
     Add Book
    </button>
  </form>
      <footer class="flex flex-row flex-center">
        <a href="#" id="logout" class="button button-primary">
          Sign Out
        </a>
      </footer>
    </aside>
  </div>
</main>`;

// Add a new user to the list
const addUser = user => {
  const userList = document.querySelector('.userList');

  if (userList) {
    // Add the user to the list
    userList.innerHTML += `<li>
      <a class="block relative" href="#">

        <span class="absolute username">${user.id} ---- ${user.name} ---- ${user.email}</span>
      </a>
    </li>`;

    // Update the number of users
    const userCount = document.querySelectorAll('.userList li').length;

    document.querySelector('.onlineCount').innerHTML = userCount;
  }
};

// Renders a books to the page
const addBooks = book => {
  // The user that sent this message (added by the populate-user hook)
  const bookList = document.querySelector('.bookList');
  if (bookList) {
    // Add the user to the list
    bookList.innerHTML += `<li>
      <a class="block relative" href="#">

        <span class="absolute username">${book.id} ---- ${book.title} ---- ${book.author}</span>
      </a>
    </li>`;

    // Update the number of books
    const bookCount = document.querySelectorAll('.bookList li').length;

    document.querySelector('.onlineCount2').innerHTML = bookCount;
  }
};

// Show the login page
const showLogin = error => {
  if (document.querySelectorAll('.login').length && error) {
    document
      .querySelector('.heading')
      .insertAdjacentHTML(
        'beforeend',
        `<p>There was an error: ${error.message}</p>`
      );
  } else {
    document.getElementById('app').innerHTML = loginHTML;
  }
};

// Shows the chat page
const showUserPage = async () => {
  document.getElementById('app').innerHTML = userHTML;

  // Find the latest 25 messages. They will come with the newest first
  const books = await client.service('books').find({
    query: {
      $sort: { id: 1 },
      $limit: 25
    }
  });
  // We want to show the newest message last
  books.data.forEach(addBooks);

  // Find all users
  const users = await client.service('users').find();

  // Add each user to the list
  users.data.forEach(addUser);
};

// Retrieve email/password object from the login/signup page
const getCredentials = () => {
  const user = {
    name: document.querySelector('[name="name"]').value,
    email: document.querySelector('[name="email"]').value,
    password: document.querySelector('[name="password"]').value
  };

  return user;
};

const getLoginCredentials = () => {
  const user = {
    email: document.querySelector('[name="email"]').value,
    password: document.querySelector('[name="password"]').value
  };

  return user;
};

// Log in either using the given email/password or the token from storage
const login = async credentials => {
  try {
    if (!credentials) {
      // Try to authenticate using an existing token
      await client.reAuthenticate();
    } else {
      // Otherwise log in with the `local` strategy using the credentials we got
      await client.authenticate({
        strategy: 'local',
        ...credentials
      });
    }

    // If successful, show the chat page
    showUserPage();
  } catch (error) {
    // If we got an error, show the login page
    showLogin(error);
  }
};

const addEventListener = (selector, event, handler) => {
  document.addEventListener(event, async ev => {
    if (ev.target.closest(selector)) {
      handler(ev);
    }
  });
};

// "Signup and login" button click handler
addEventListener('#signup2', 'click', async () => {
  // For signup, create a new user and then log them in
  const credentials = getCredentials();

  // First create the user
  await client.service('users').create(credentials);
  // If successful log them in
  await login(credentials);
});

// "Login" button click handler
addEventListener('#login', 'click', async () => {
  const user = getLoginCredentials();
  console.log('User Here');
  await login(user);
});

// "Login" button click handler
addEventListener('#signupPage', 'click', async () => {
  document.getElementById('app').innerHTML = signupHTML;
});
// "Logout" button click handler
addEventListener('#logout', 'click', async () => {
  await client.logout();

  document.getElementById('app').innerHTML = loginHTML;
});

// "login" link in signup page button click handler
addEventListener('#login2', 'click', async () => {
  await client.logout();

  document.getElementById('app').innerHTML = loginHTML;
});

// Add book to databsae
addEventListener('#addBook', 'click', async () => {
  // This is the message text input field
  var title = document.querySelector('[name="title"]').value;
  var author = document.querySelector('[name="author"]').value;
  var u_id = document.querySelector('[name="userId"]').value;
  //ev.preventDefault();

  // Create a new message and then clear the input field
  await client.service('books').create({
    title,
    author,
    u_id
  });

  console.log(title, '  ', author, '  ', u_id);
  document.querySelector('[name="title"]').value = '';
  document.querySelector('[name="author"]').value = '';
  document.querySelector('[name="userId"]').value = '';
});

// Listen to created events and add the new message in real-time
client.service('books').on('created', addBooks);

// We will also see when new users get created in real-time
client.service('users').on('created', addUser);

// Call login right away so we can show the chat window
// If the user can already be authenticated
login();
