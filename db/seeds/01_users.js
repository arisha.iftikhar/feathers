exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users')
    .del()
    .then(function() {
      // Inserts seed entries
      var bcrypt = require('bcryptjs');
      return knex('users').insert([
        {
          id: 1,
          name: 'Hankang',
          email: 'hankang@gmail.com',
          password: bcrypt.hashSync('11223344', 10)
        },
        {
          id: 2,
          name: 'Somerset',
          email: 'somerset@gmail.com',
          password: bcrypt.hashSync('abcde', 10)
        },
        {
          id: 3,
          name: 'G.B.Shaw',
          email: 'g.b.shaw@gmail.com',
          password: bcrypt.hashSync('123456', 10)
        },
        {
          id: 4,
          name: 'Homer',
          email: 'homer@gmail.com',
          password: bcrypt.hashSync('123456', 10)
        },
        {
          id: 5,
          name: 'Homer2',
          email: 'homer2@gmail.com',
          password: bcrypt.hashSync('123456', 10)
        }
      ]);
    });
};
