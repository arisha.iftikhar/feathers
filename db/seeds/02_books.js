exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex("books")
    .del()
    .then(function() {
      // Inserts seed entries
      return knex("books").insert([
        { id: 1, title: "Magician", author: "Somerset", u_id: "1" },
        { id: 2, title: "Invisible", author: "H.G.Wells", u_id: "2" },
        { id: 3, title: "Apple Cart", author: "G.B.Shaw", u_id: "3" },
        { id: 4, title: "Apple Cart2", author: "G.B.Shaw", u_id: "3" }
      ]);
    });
};
