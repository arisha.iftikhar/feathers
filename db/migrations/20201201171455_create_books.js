exports.up = function(knex) {
  return knex.schema.createTable("books", function(table) {
    table.increments("id").primary();
    table.string("title").notNullable();
    table.string("author").notNullable();
    table.integer("u_id");
    table.foreign("u_id").references("users.id");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("books");
};
